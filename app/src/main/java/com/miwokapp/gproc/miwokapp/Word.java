package com.miwokapp.gproc.miwokapp;

public class Word {
    private String defaultTranslation;
    private String miwokTranslation;
    private int imageResourceId;
    private int audioResouceId;

    public Word(String defaultTranslation, String miwokTranslation, int imageResourceId, int audioResouceId){
        this.defaultTranslation = defaultTranslation;
        this.miwokTranslation = miwokTranslation;
        this.imageResourceId = imageResourceId;
        this.audioResouceId = audioResouceId;
    }

    public String GetDefaultTranslation(){
        return defaultTranslation;
    }

    public String GetMiwokTranslation(){
        return miwokTranslation;
    }

    public int GetImageResourceId(){
        return imageResourceId;
    }

    public int GetAudioResourceId() { return audioResouceId; }
}
