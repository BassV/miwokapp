package com.miwokapp.gproc.miwokapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class WordAdapter extends ArrayAdapter<Word> {
    public WordAdapter(Activity context, ArrayList<Word> word){
        super(context, 0, word);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.number_list_item, parent, false);
        }

        Word currentWord = getItem(position);

        TextView defaultTranslation = listItemView.findViewById(R.id.default_textView);
        defaultTranslation.setText(currentWord.GetDefaultTranslation());

        TextView miwokTranslation = listItemView.findViewById(R.id.miwok_textView);
        miwokTranslation.setText(currentWord.GetMiwokTranslation());

        ImageView itemIcon = listItemView.findViewById(R.id.item_icon);
        itemIcon.setImageResource(currentWord.GetImageResourceId());

        return listItemView;
    }
}
