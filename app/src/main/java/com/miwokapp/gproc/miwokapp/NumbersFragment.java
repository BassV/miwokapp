package com.miwokapp.gproc.miwokapp;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NumbersFragment extends Fragment {

    public NumbersFragment() {
        // Required empty public constructor
    }

    private MediaPlayer mMediaPlayer;
    private AudioManager mAudioManager;

    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {
                    if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                            focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                        // AUDIOFOCUS_LOSS_TRANSIENT means that we lost audio focus for a short time
                        // AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK means that out app is allowed to play a sound at a lower volume
                        // Pause playback
                        mMediaPlayer.pause();
                        mMediaPlayer.seekTo(0); // Set the audiofile position to its beginning
                    } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN){
                        // AUDIOFOCUS_GAIN means that we have regained focus and can resume out action
                        // Resume playback
                        mMediaPlayer.start();
                    } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS){
                        // AUDIOFOCUS_LOSS means that we permanently lost audiofocus
                        // Stop playback
                        ReleaseMediaPlayer();
                    }
                }
            };

    // Instead of creating a completion listener for every list item, only do it once
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            ReleaseMediaPlayer();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_numbers, container, false);

        // Create and setup audiomanager to request audio focus
        mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

        final ArrayList<Word> words = new ArrayList<>();
        words.add(new Word("one", "lutti", R.drawable.billy, R.raw.number_one));
        words.add(new Word("two", "otiiko", R.drawable.billy, R.raw.number_two));
        words.add(new Word("three", "tolookosu", R.drawable.billy, R.raw.number_three));
        words.add(new Word("four", "oyyisa", R.drawable.billy, R.raw.number_four));
        words.add(new Word("five", "massokka", R.drawable.billy, R.raw.number_five));
        words.add(new Word("six", "temmokka", R.drawable.billy, R.raw.number_six));
        words.add(new Word("seven", "kenekaku", R.drawable.billy, R.raw.number_seven));
        words.add(new Word("eight", "kawinta", R.drawable.billy, R.raw.number_eight));
        words.add(new Word("nine", "wo'e", R.drawable.billy, R.raw.number_nine));
        words.add(new Word("ten", "na'aacha", R.drawable.billy, R.raw.number_ten));

        WordAdapter adapter = new WordAdapter(getActivity(), words);

        ListView listView = rootView.findViewById(R.id.numbers_list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Word word = words.get(position);

                // Release the media player before a new sound is played
                ReleaseMediaPlayer();

                // Request audio focus for playback
                int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                        // Use the music stream
                        AudioManager.STREAM_MUSIC,
                        // Request a short focus, pause previous holder from playing
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                // If request granted, start playing the sound
                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED){
                    mMediaPlayer = MediaPlayer.create(getActivity(), word.GetAudioResourceId());
                    mMediaPlayer.start();

                    // Release the media when an audio finishes playing
                    mMediaPlayer.setOnCompletionListener(mCompletionListener);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        // Release the media player resources whenever the user leaves the app
        ReleaseMediaPlayer();
    }

    private void ReleaseMediaPlayer(){
        if (mMediaPlayer != null){
            mMediaPlayer.release();
            mMediaPlayer = null;

            // Abandon audio focus when the audio needs to be released (audio is stopped)
            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
        }
    }
}
